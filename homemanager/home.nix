# SPDX-FileCopyrightText: 2024 Simon Hornbachner <shr@calliotec.com>
#
# SPDX-License-Identifier: CC0-1.0

{ config, pkgs, ... }:

let
  # personal info
  name       = "Simon Hornbachner";
  real_email = "shr@calliotec.com";
  spam_email = "sih@anche.no";

in {

  home.username = "shr";
  home.homeDirectory = "/home/shr";

  home.stateVersion = "23.11"; # Please read the comment before changing.

  # packages to install
  home.packages = with pkgs; [
    brave
    helix
    keepassxc
    libreoffice-fresh
    reuse
    signal-desktop
    tmux
    ungoogled-chromium
  ];

  # files to copy into nix store and link to home
  home.file = {
    ".ssh/config".source = ssh/sshconfig;
  };

  home.sessionVariables = {
    EDITOR = "helix";
  };

  # Let homemanager install and manage itself.
  programs.home-manager.enable = true;

  # Don't let homemanager manage bash
  programs.bash = {
    enable = false;
    enableCompletion = true;
  };

  # Let homemanager create a gitconfig
  programs.git = {
    enable = true;
    userName = "${name}";
    userEmail = "${real_email}";
    aliases = {
      a = "add";
      b = "branch";
      c = "commit";
      cm = "commit -m";
      co = "checkout";
      d = "diff";
      f = "fetch";
      l = "log --oneline";
      lv = "log";
      m = "merge";
      p = "push";
      pl = "pull";
      r = "rebase";
      s = "status";
    };
    extraConfig = {
      init.defaultBranch = "main";
    };
  };

  home.shellAliases = {
    "…" = "cd ..";
    "……" = "cd ../..";
    "………" = "cd ../../..";
    "…………" = "cd ../../../..";
    "……………" = "cd ../../../../..";
    g = "git";
    grep="grep --colour=auto";
    ls="ls --color=auto";
    v = "vagrant";
  };

  targets.genericLinux.enable = true;
}
