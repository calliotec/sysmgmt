<!--
SPDX-FileCopyrightText: 2024 Simon Hornbachner <shr@calliotec.com>

SPDX-License-Identifier: CC0-1.0
-->

shr GNU+Linux/Unix Dotfiles
===========================

This repository contains system management and configuration for Linux
(and in the future potentially other Unix) systems.

## License
To the extent possible under law, the person who associated CC0 with this work
has waived all copyright and related or neighboring rights to this work.
This work is published from: Austria.

