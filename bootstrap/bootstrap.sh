# SPDX-FileCopyrightText: 2024 Simon Hornbachner <shr@calliotec.com>
#
# SPDX-License-Identifier: CC0-1.0

# debug options
#export PS4="\$LINENO: "
#set -xv

# Check whether a command exists - returns 0 if it does, 1 if it does not
exists() {
  if command -v "${1}" >/dev/null 2>&1
  then
    return 0
  else
    return 1
  fi
}

if test -f /etc/arch-release
then
  INSTALL_CMD="sudo pacman --sync --refresh --noconfirm"
elif grep -q "ManjaroLinux" /etc/lsb-release
then
  INSTALL_CMD="sudo pacman --sync --refresh --noconfirm"
elif grep -q "Ubuntu" /etc/lsb-release
then
  echo "updating apt sources"
  sudo apt-get --assume-yes update
  INSTALL_CMD="sudo apt-get --assume-yes install"
else
  echo "Currently supported: Arch, Manjaro, Ubuntu. Exiting..."
  exit 1
fi

echo "Creating ~/.ssh if it doesn't exist already..."
if ! test -d "${HOME}"/.ssh
then
  mkdir "${HOME}"/.ssh
  chmod 0700 "${HOME}"/.ssh
fi

if test -f "${HOME}"/.ssh/id_ed25519_argo
then
  echo "Looks like SSH private key for argo is already installed. Skipping cp..."
else
  echo "Attempting to copy argo SSH keys..."
  sshkey="$(pwd)"/id_ed25519_argo
  if ! test -f "${sshkey}"
  then
    echo "SSH private key for argo git not found in current directory. Exiting..."
    exit 1
  fi
  cp "${sshkey}" "${HOME}"/.ssh/
  cp "${sshkey}.pub" "${HOME}"/.ssh/
  sudo chown "$(id -u)":"$(id -g)" "${HOME}"/.ssh/id*
  sudo chmod 0600 "${HOME}"/.ssh/id*
  echo "SSH keys copied!"
fi

echo "Creating ${HOME}/.ssh/config if it doesn't exist already..."
if ! test -f "${HOME}"/.ssh/config
then
  touch "${HOME}"/.ssh/config
fi

if ! grep -q "git.argo.coop" "${HOME}"/.ssh/config
then
  cat<<EOT >> "${HOME}"/.ssh/config
Host git.argo.coop
	IdentityFile ~/.ssh/id_ed25519_argo
	IdentitiesOnly yes
EOT
fi

if ! test -f "${HOME}"/.ssh/known_hosts
then
  touch "${HOME}"/.ssh/known_hosts
fi

if ! grep -q "git.argo.coop" "${HOME}"/.ssh/known_hosts
then
  cat<<EOT >> "${HOME}"/.ssh/known_hosts
git.argo.coop ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKftfznQRhdn2yAiYwEQ5Q5ZKZR1sz93AdIhFad11EMb
EOT
fi

if ! exists git
then
  echo "Git appears not to be installed. Attempting installation now..."
  ${INSTALL_CMD} git
fi

echo "Cloning dotfiles.git and running nix-bootstrap.sh"

if ! test -d "${HOME}/hacking"
then
  mkdir "${HOME}/hacking"
fi

if ! test -d "${HOME}/hacking/dotfiles.git"
then
  git clone gitea@git.argo.coop:infra-cyman/dotfiles.git "${HOME}/hacking/dotfiles.git"
fi
bash "${HOME}/hacking/dotfiles.git/bootstrap/nix-bootstrap.sh"

source /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh
source "${HOME}/.nix-profile/etc/profile.d/hm-session-vars.sh"

home-manager switch -b backup --flake "${HOME}"/hacking/dotfiles.git/

