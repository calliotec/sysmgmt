# SPDX-FileCopyrightText: 2024 Simon Hornbachner <shr@calliotec.com>
#
# SPDX-License-Identifier: CC0-1.0

# debug options
#export PS4="\$LINENO: "
#set -xv

# Check whether a command exists - returns 0 if it does, 1 if it does not
exists() {
  if command -v "$1" >/dev/null 2>&1
  then
    return 0
  else
    return 1
  fi
}

echo "Checking distribution..."
if test -f /etc/arch-release
then
  INSTALL_CMD="sudo pacman --sync --refresh --noconfirm"
elif grep -q "ManjaroLinux" /etc/lsb-release
then
  INSTALL_CMD="sudo pacman --sync --refresh --noconfirm"
elif grep -q "Ubuntu" /etc/lsb-release
then
  echo "updating apt sources"
  sudo apt-get --assume-yes update
  INSTALL_CMD="sudo apt-get --assume-yes install"
else
  echo "Currently supported: Arch, Manjaro, Ubuntu. Exiting..."
  exit 1
fi

if ! exists curl
then
  echo "Curl appears not to be installed. Attempting installation now..."
  ${INSTALL_CMD} curl
fi

echo "Checking if nix is already installed..."
if ! exists nix
then
  echo "nix appears not to be installed, downloading and running installer..."
  tmp_dir=$(mktemp -d)
  cd "${tmp_dir}"

  curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix > nix-installer.sh
  bash nix-installer.sh install --no-confirm --diagnostic-endpoint=""

  source /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh

  echo "Installing and initializing home-manager"
  nix run home-manager/master -- init --switch .
  source "${HOME}/.nix-profile/etc/profile.d/hm-session-vars.sh"
  home-manager switch -b backup --flake ./

  echo "Performing cleanup..."
  rm -rf "${tmp_dir}"
fi
echo "Done"

